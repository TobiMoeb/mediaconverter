﻿Function Get-FileName($initialDirectory)
{
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
    
    $OpenFileDialog = New-Object System.Windows.Forms.FolderBrowserDialog
    
    $OpenFileDialog.Description = "Ordner mit MP4 Dateien auswählen";
    $OpenFileDialog.ShowNewFolderButton = 0;
    #$OpenFileDialog.RootFolder = Environment.SpecialFolder.Personal;

    $OpenFileDialog.ShowDialog() | Out-Null
    $OpenFileDialog.SelectedPath
}

Function ProcessMp4
{
    foreach($file in $input) 
    { 

        $newDirectory = [io.path]::Combine($file.DirectoryName, "mp4")
        [io.directory]::CreateDirectory($newDirectory) | Out-Null

        $newFileName = [io.path]::Combine($newDirectory, $file.Name)
        $newFileName = ([io.path]::ChangeExtension($newFileName, "mp3"))

        Write-Host $newFileName

        & .\ffmpeg.exe -i $file.FullName -q:a 0 -map a $newFileName -hide_banner -loglevel panic -y
    }
}

$inputfile = Get-FileName "C:\Users\Tobi\Music"
Write-Host $inputfile
Get-ChildItem -Recurse -filter *.mp4 -Path $inputfile | ProcessMp4
Write-Host "Fertig"
Read-Host
